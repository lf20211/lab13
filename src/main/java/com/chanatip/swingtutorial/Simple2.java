package com.chanatip.swingtutorial;

import javax.swing.*;

class MyFrame extends JFrame {
    
    JButton button;
    public MyFrame() {
        super("Frist JFrame");
        button = new JButton("Click");
        button.setBounds(130, 100, 100, 40);
        this.setLayout(null);
        this.add(button);
        this.setSize(400, 500);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }
}

public class Simple2 {
    public static void main(String[] args) {
        MyFrame frame = new MyFrame();
        frame.setVisible(true);
    }
}
